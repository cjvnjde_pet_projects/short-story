import React, { useRef, useState } from "react";
import { TextContainer } from "./TextBody.style";
import { PopUp } from "../../components/PopUp/PopUp";
import { setCORS } from "google-translate-api-browser";
const translate = setCORS("https://cors-anywhere.herokuapp.com/");
// console.log(translate);
export const TextBody = () => {
  const [position, setPosition] = useState({
    left: 0,
    top: 0,
    height: 0,
    width: 0
  });

  const [selectedText, setSelectedText] = useState("");

  const getSelectedWord = (e: React.MouseEvent) => {
    const selection = window.getSelection();
    const oRange = selection.getRangeAt(0);
    const oRect = oRange.getBoundingClientRect();
    setPosition({
      left: oRect.left,
      top: oRect.top,
      height: oRect.height,
      width: oRect.width
    });

    let text = selection.focusNode
      .textContent!.substring(selection.focusOffset, selection.anchorOffset)
      .trim();
    translate(text, { to: "ru" }).then((res: any) => {
      setSelectedText(res.text);
    });
  };

  return (
    <React.Fragment>
      <PopUp position={position} text={{ translates: [selectedText] }} />
      <TextContainer
        onDoubleClick={getSelectedWord}
        onClick={() => {
          setSelectedText("");
          setPosition({
            left: 0,
            top: 0,
            height: 0,
            width: 0
          });
        }}
      >
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Atque quia
        voluptatum amet enim eaque fugiat consequuntur culpa sed tempora tempore
        molestiae praesentium id ipsam totam a deleniti, ipsa perferendis iure.
        Voluptatem, iste! Placeat minima nam dolor facilis beatae quis, laborum
        officiis cumque soluta cupiditate, veniam ratione quod? Dolore sapiente
        fuga aut odit! Molestias ratione, cumque fugiat numquam, inventore
        itaque id expedita incidunt excepturi ad, officiis dicta maxime
        provident repudiandae eveniet asperiores sunt illo earum animi harum?
        Quis est odit esse deserunt optio laborum, iure dolorem aspernatur quae
        reprehenderit ea error libero odio totam, officia quia rem sit
        perspiciatis, excepturi magni?
      </TextContainer>
    </React.Fragment>
  );
};
