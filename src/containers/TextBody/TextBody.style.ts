import styled from "styled-components";

export const TextContainer = styled.div`
  background-color: red;
  margin: 0.5rem;
  padding: 0.2rem;
  border-radius: 0.3rem;
`;
