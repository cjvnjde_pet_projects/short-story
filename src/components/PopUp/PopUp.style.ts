import styled from "styled-components";
import { Position } from "./PopUp";
interface IProps {
  position: Position;
  count: number;
}
export const Cloud = styled.div`
  position: absolute;
  display: flex;
  flex-direction: column;
  left: ${(props: IProps) => `${props.position.left - 18 * 0.2}px`};
  top: ${(props: IProps) =>
    `${props.position.top - 20 * props.count - 18 * 0.2}px`};
  /* height: ${(props: IProps) => `${props.position.height}px`}; */
  width: auto;
  padding: 0.2rem;
  border: 1px solid gray;
  border-radius: 0.2rem;
  color: green;
  background-color: white;
  p {
    margin: 0;
    padding: 0;
  }
`;
