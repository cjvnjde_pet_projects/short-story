import React from "react";
import { Cloud } from "./PopUp.style";

export interface Position {
  left: number;
  top: number;
  height: number;
  width: number;
}
interface IProps {
  text: {
    translates: string[];
  };
  position: Position;
}

export const PopUp = ({ text, position }: IProps) => {
  return (
    <Cloud position={position} count={text.translates.length}>
      {text.translates.map(text => (
        <p key={text}>{text}</p>
      ))}
    </Cloud>
  );
};
