import React from "react";
import ReactDOM from "react-dom";
import { App } from "./App";
import { createGlobalStyle } from "styled-components";

const GlobalStyle = createGlobalStyle`
  body {
    margin: 0;
    padding: 0;
  }
`;

ReactDOM.render(
  React.createElement(
    React.Fragment,
    null,
    React.createElement(GlobalStyle),
    React.createElement(App)
  ),
  document.getElementById("root")
);
