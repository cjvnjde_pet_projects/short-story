import React from "react";
import { Header } from "./components/Header/Header";
import { Footer } from "./components/Footer/Footer";
import { TextBody } from "./containers/TextBody/TextBody";

export const App = () => {
  return (
    <React.Fragment>
      <Header />
      <TextBody />
      <Footer />
    </React.Fragment>
  );
};
